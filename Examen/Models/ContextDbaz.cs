﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Examen.Models
{
    public class ContextDbaz : DbContext
    {
        public ContextDbaz()
            : base(@"Data Source=KINGWIN8\SQLEXPRESS;Initial Catalog=aspExam;Integrated Security=True")
        {

        }

        public DbSet<Entreprise> entreprises { get; set; }
        public DbSet<Prestation> prestations { get; set; }        
    }
}