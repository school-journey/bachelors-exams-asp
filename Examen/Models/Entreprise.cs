﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Examen.Models
{


    public class Entreprise
    {

        [Key]
        public int id { get; set; }

        [Required]
        [DisplayName("Nom")]
        public virtual string nom { get; set; }

        [Required]
        [DisplayName("Raison sociale")]
        public virtual string rzonSociale { get; set; }

        [Required]
        [DisplayName("Adresse")]
        public virtual string adresse { get; set; }  
    }
}