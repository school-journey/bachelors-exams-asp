﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Examen.Models
{
    public class Prestation
    {
        [Key]
        public virtual int id { get; set; }

        [DisplayName("Entreprise")]
        public virtual int idEntreprise { get; set; }

        [ForeignKey("idEntreprise")]
        [Required]
        [DisplayName("Entreprise")]
        public virtual Entreprise entreprise { get; set; }     

        [Required]
        [DisplayName("Description")]
        public virtual string description { get; set; }

        [Required]
        [DisplayName("Nom")]
        public virtual string nom { get; set; }

        [Required]
        [DisplayName("Montant")]
        public virtual float montant { get; set; }

        [Required]
        [DisplayName("Date")]
        public virtual DateTime date { get; set; }
    }
}