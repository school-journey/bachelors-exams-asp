﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    public class PrestationController : Controller
    {
        private ContextDbaz db = new ContextDbaz();

        // GET: /Prestation/
        public ActionResult Index()
        {
            var inscriptions = db.prestations.Include(p => p.entreprise);
            return View(inscriptions.ToList());
        }

        // GET: /Prestation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestation prestation = db.prestations.Find(id);
            if (prestation == null)
            {
                return HttpNotFound();
            }
            return View(prestation);
        }

        // GET: /Prestation/Create
        public ActionResult Create()
        {
            ViewBag.idEntreprise = new SelectList(db.entreprises.ToList(), "id", "nom");
            return View();
        }

        // POST: /Prestation/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,idEntreprise,description,nom,montant,date")] Prestation prestation)
        {
            ViewBag.idEntreprise = new SelectList(db.entreprises.ToList(), "id", "nom");

            var enter = (from ent in db.entreprises where ent.id == prestation.idEntreprise select ent).FirstOrDefault();
            prestation.entreprise = enter;

            try
            {
                db.prestations.Add(prestation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }catch(Exception ee)
            {
                 return View("error");
            }
        }


        // GET: /Prestation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestation prestation = db.prestations.Find(id);
            if (prestation == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEntreprise = new SelectList(db.entreprises.ToList(), "id", "nom", prestation.idEntreprise);
            return View(prestation);
        }

        // POST: /Prestation/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,idEntreprise,description,nom,montant,date")] Prestation prestation)
        {
            ViewBag.idEntreprise = new SelectList(db.entreprises.ToList(), "id", "nom");
            var enter = (from ent in db.entreprises where ent.id == prestation.idEntreprise select ent).FirstOrDefault();
            prestation.entreprise = enter;
            prestation.montant = 1000;
          
            //var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));
            //if (ModelState.IsValid)
            //{
                try
                {
                    db.Entry(prestation).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                
                }
            //}
            prestation.description = "";
            ViewBag.idEntreprise = new SelectList(db.entreprises.ToList(), "id", "nom", prestation.idEntreprise);
            return View(prestation);
        }

        // GET: /Prestation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestation prestation = db.prestations.Find(id);
            if (prestation == null)
            {
                return HttpNotFound();
            }
            return View(prestation);
        }

        // POST: /Prestation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Prestation prestation = db.prestations.Find(id);
            db.prestations.Remove(prestation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
