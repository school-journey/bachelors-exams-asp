﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(nawetAsp.Startup))]
namespace nawetAsp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
