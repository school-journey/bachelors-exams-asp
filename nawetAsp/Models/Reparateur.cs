﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nawetAsp.Models
{
    public class Reparateur
    {
        [Key]
        public virtual int id { get; set; }

        [Required]
        [DisplayName("Code")]
        public virtual string code { get; set; }

        [DisplayName("Nom")]
        public virtual string nom { get; set; }

        [Required]
        [DisplayName("Telephone")]
        public virtual string tel { get; set; }
    }
}