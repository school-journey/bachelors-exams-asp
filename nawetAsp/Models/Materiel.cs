﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nawetAsp.Models
{
    public class Materiel
    {
        [Key]
        public virtual int id { get; set; }

        [Required]
        [DisplayName("Reference")]
        public virtual string reference { get; set; }

        [Required]
        [DisplayName("Marque")]
        public virtual string marque { get; set; }
    }
}