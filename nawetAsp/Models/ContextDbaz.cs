﻿using nawetAsp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Examen.Models
{
    public class ContextDbaz : DbContext
    {
        public ContextDbaz()
            : base(@"Data Source=KINGWIN8\SQLEXPRESS;Initial Catalog=nawetAsp;Integrated Security=True")
        {

        }

        public DbSet<Materiel> materiel { get; set; }
        public DbSet<Reparateur> reparateur  { get; set; }
        public DbSet<Reparation> reparation { get; set; }

    }
}