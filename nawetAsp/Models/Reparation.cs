﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace nawetAsp.Models
{
    public class Reparation
    {
        [Key]
        public virtual int id { get; set; }

        [Required]
        [DisplayName("Description")]
        public virtual string description { get; set; }

        [Required]
        [DisplayName("Montant")]
        public virtual int montant { get; set; }

        [Required]
        [DisplayName("Date")]
        public virtual DateTime date { get; set; }

        //--------------
        [DisplayName("Materiel")]
        public virtual int idMateriel { get; set; }

        [ForeignKey("idMateriel")]
        [Required]
        public virtual Materiel materiel { get; set; }

        //-------------
        [DisplayName("Reparateur")]
        public virtual int idReparateur { get; set; }

        [ForeignKey("idReparateur")]
        [Required]
        public virtual Reparateur reparateur { get; set; }

    }
}