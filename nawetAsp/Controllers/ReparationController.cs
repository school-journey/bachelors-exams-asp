﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using nawetAsp.Models;
using Examen.Models;

namespace nawetAsp.Controllers
{
    public class ReparationController : Controller
    {
        private ContextDbaz db = new ContextDbaz();

        // GET: /Reparation/
        public ActionResult Index()
        {
            var reparation = db.reparation.Include(r => r.materiel).Include(r => r.reparateur);
            return View(reparation.ToList());
        }

        public ActionResult Recherche(string date)
        {
            if(date.Trim().Length > 0)
            {
                var reparation = db.reparation.Include(r => r.materiel).Include(r => r.reparateur);
                DateTime d = DateTime.Parse(date);
                var re = (from e in db.reparation
                          where d.CompareTo(e.date) == 0
                          select e).ToList();

                return View(re);
            }
            return RedirectToAction("Index");
        }

        // GET: /Reparation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reparation reparation = db.reparation.Find(id);
            if (reparation == null)
            {
                return HttpNotFound();
            }
            return View(reparation);
        }

        // GET: /Reparation/Create
        public ActionResult Create()
        {
            ViewBag.idMateriel = new SelectList(db.materiel, "id", "reference");
            ViewBag.idReparateur = new SelectList(db.reparateur, "id", "code");
            return View();
        }

        // POST: /Reparation/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,description,montant,date,idMateriel,idReparateur")] Reparation reparation)
        {
            try
            {
                ViewBag.idMateriel = new SelectList(db.materiel, "id", "reference");
                ViewBag.idReparateur = new SelectList(db.reparateur, "id", "code");
                ////if (ModelState.IsValid)
                ////{
                Reparateur rep = db.reparateur.Find(reparation.idReparateur);
                Materiel mat = db.materiel.Find(reparation.idMateriel);
                reparation.reparateur = rep;
                reparation.materiel = mat;



                db.reparation.Add(reparation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View("error");
            }
            ////}

            //ViewBag.idMateriel = new SelectList(db.materiel, "id", "reference", reparation.idMateriel);
            //ViewBag.idReparateur = new SelectList(db.reparateur, "id", "code", reparation.idReparateur);
            //return View(reparation);
        }

        // GET: /Reparation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reparation reparation = db.reparation.Find(id);
            if (reparation == null)
            {
                return HttpNotFound();
            }
            ViewBag.idMateriel = new SelectList(db.materiel, "id", "reference", reparation.idMateriel);
            ViewBag.idReparateur = new SelectList(db.reparateur, "id", "code", reparation.idReparateur);
            return View(reparation);
        }

        // POST: /Reparation/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,description,montant,date,idMateriel,idReparateur")] Reparation reparation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reparation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idMateriel = new SelectList(db.materiel, "id", "reference", reparation.idMateriel);
            ViewBag.idReparateur = new SelectList(db.reparateur, "id", "code", reparation.idReparateur);
            return View(reparation);
        }

        // GET: /Reparation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reparation reparation = db.reparation.Find(id);
            if (reparation == null)
            {
                return HttpNotFound();
            }
            return View(reparation);
        }

        // POST: /Reparation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reparation reparation = db.reparation.Find(id);
            db.reparation.Remove(reparation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
