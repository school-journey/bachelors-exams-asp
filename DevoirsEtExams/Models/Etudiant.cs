﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class Etudiant
    {
        [Key]
        public int id { get; set; }

        public string matricule { get; set; }
        
        [DisplayName("Nom")]
        public string nom { get; set; }

        [DisplayName("Etudiant")]
        public string tel { get; set; }

        [DisplayName("Date Naissance")]
        public DateTime naissance { get; set; }
    }
}