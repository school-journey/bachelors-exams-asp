﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class Inscription
    {
        [DisplayName("Etudiant")]
        [Required]
        public virtual Etudiant etudiant { get; set; }

        [DisplayName("Classe")]
        [Required]
        public virtual Classe classe { get; set; }

        [DisplayName("Annee inscription")]
        [Required]
        public int annee { get; set; }

        [DisplayName("Date inscription")]
        [Required]
        public DateTime dateInscription { get; set; }
    }
}