﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevoirsEtExams.Startup))]
namespace DevoirsEtExams
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
