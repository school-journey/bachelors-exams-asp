﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Devoir.Startup))]
namespace Devoir
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
