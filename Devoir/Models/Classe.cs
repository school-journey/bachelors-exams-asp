﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class Classe
    {
        [Key]
        public int id { get; set; }

        public string nom { get; set; }
    }
}