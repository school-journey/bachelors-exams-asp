﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class Etudiant
    {
        [Key]
        public int id { get; set; }

        [Required]
        [DisplayName("Matricule")][StringLength(25)]
        [Index("indexUn", IsUnique=true)]
        public string matricule { get; set; }
        
        [DisplayName("Nom")]
        [Required]
        public string nom { get; set; }

        [Phone]
        [DisplayName("Telephone")]
        public string tel { get; set; }

        [DisplayName("Date Naissance")]
        public DateTime naissance { get; set; }
    }
}