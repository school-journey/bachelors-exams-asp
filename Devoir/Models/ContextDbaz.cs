﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class ContextDbaz : DbContext
    {
        public ContextDbaz()
            : base(@"Data Source=KINGWIN8\SQLEXPRESS;Initial Catalog=aspDev;Integrated Security=True")
        {

        }

        public DbSet<Etudiant> Etudiants { get; set; }
        public DbSet<Inscription> Inscriptions { get; set; }
        public DbSet<Classe> classes { get; set; }

    }
}