﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace DevoirsEtExams.Models
{
    public class Inscription
    {     
        [Key]
        public int id { get; set; }

//        [Key]               //La clée etrangere plus l'année constituent la clée  primaire
 //       [Column(Order = 1)]    //Sans ça la concatenation de clée ne marchera pas
        //[Index("anneeIdEtudiant", Order = 1, IsUnique = true)]        // Index qui regroupe plusieurs colonnes, 1 est le numero d'ordre de cette colonne dans l'index;  --- Fonctionne pas avec les clees etrangeres
        [DisplayName("Etudiant")]
        public int idEtudiant { get; set; }//Clee etrangere qui tire sa clee primaire de l'objet Etudiant ci-dessous

        [ForeignKey("idEtudiant")]                                         
        [Required]
        public virtual Etudiant etudiant { get; set; }       //L'objet contenu est vue comme la cle primaire et la variable identifiant (ici idEtudiant) est la cle etrangere

        [DisplayName("Classe")]
        public int idClasse { get; set; }      //Clee etrangere qui tire sa clee primaire de l'objet Classe ci-dessous

        //[System.ComponentModel.DataAnnotations.Association("cleeEtr2", "idClasse", "id", IsForeignKey = true)]
        [ForeignKey("idClasse")]        //En reference a l'attribut qui est juste en haut ici
        [Required]
        public virtual Classe classe { get; set; }
        //L'objet contenu est vue comme la cle primaire et la variable identifiant (ici idClasse) est la cle etrangere


        //Annee
        //[Key]
        //[Column(Order = 2)]            //Sans ça la concatenation de clée ne fonctionne pas
        //[DisplayName("Annee inscription")]
        //[Required]
        //[Index("anneeIdEtudiant", Order = 2, IsUnique = true)]        // Index qui regroupe plusieurs colonnes, 2 est le numero d'ordre de cette colonne dans l'index
        //[Column(Order = 2)]
        //[StringLength(25)]
        public int annee { get; set; }

        //Date d'inscription
        [DisplayName("Date inscription")]
        [Required]
        public DateTime dateInscription { get; set; }
    }
}