﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevoirsEtExams.Models;

namespace Devoir.Controllers
{
    public class EtudiantController : Controller
    {
        private ContextDbaz db = new ContextDbaz();

        // GET: /Etudiant/
        public ActionResult Index()
        {
            var r = from e in db.Etudiants select e;
            return View(db.Etudiants.ToList());
        }

        public ActionResult listeEtudAll()
        {            
                var r = (from e in db.Etudiants select e).ToList();

                return PartialView(r);            
        }

        public ActionResult listeEtud(DateTime? date)
        {
            if(date.HasValue)
            {
                var r = (from e in db.Etudiants where e.naissance >= date select e).ToList();

                return View(r);
            }
            return View("Error");
        }

        public ActionResult Recherche()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Recherche(string date)
        {
            try
            {
                DateTime d = DateTime.Parse(date);
                var r = (from e in db.Etudiants where d.CompareTo(e.naissance) == 1
                         || d.CompareTo(e.naissance) == 0
                         select e).ToList();
               
                ViewBag.li = r;

                return View("Result");
            }catch(Exception ex)
            {

            }


            return View("Error");
        }

        // GET: /Etudiant/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
 
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etudiant etudiant = db.Etudiants.Find(id);
            if (etudiant == null)
            {
                return HttpNotFound();
            }
            return View(etudiant);
        }

        // GET: /Etudiant/Create
        public ActionResult Create()
        {
                return View();
        }

        // POST: /Etudiant/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,matricule,nom,tel,naissance")] Etudiant etudiant)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    db.Etudiants.Add(etudiant);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(etudiant);
            }
            catch
            {

            }
            return View("error");

        }

        // GET: /Etudiant/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etudiant etudiant = db.Etudiants.Find(id);
            if (etudiant == null)
            {
                return HttpNotFound();
            }
            return View(etudiant);
        }

        // POST: /Etudiant/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,matricule,nom,tel,naissance")] Etudiant etudiant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(etudiant).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(etudiant);
        }

        // GET: /Etudiant/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etudiant etudiant = db.Etudiants.Find(id);
            if (etudiant == null)
            {
                return HttpNotFound();
            }
            return View(etudiant);
        }

        // POST: /Etudiant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Etudiant etudiant = db.Etudiants.Find(id);
            db.Etudiants.Remove(etudiant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
